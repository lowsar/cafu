/*
=================================================================================
This file is part of Cafu, the open-source game engine and graphics engine
for multiplayer, cross-platform, real-time 3D action.
Copyright (C) 2002-2014 Carsten Fuchs Software.

Cafu is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Cafu is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Cafu. If not, see <http://www.gnu.org/licenses/>.

For support and more information about Cafu, visit us at <http://www.cafu.de>.
=================================================================================
*/

#ifndef CAFU_GAMESYS_WORLD_HPP_INCLUDED
#define CAFU_GAMESYS_WORLD_HPP_INCLUDED

#include "UniScriptState.hpp"

#include <stdexcept>


namespace cf { namespace ClipSys { class ClipWorldT; } }
namespace cf { namespace ClipSys { class CollModelManI; } }
namespace cf { namespace GuiSys { class GuiResourcesT; } }
class ModelManagerT;
class PhysicsWorldT;
struct CaKeyboardEventT;
struct CaMouseEventT;


namespace cf
{
    namespace GameSys
    {
        /// The TypeInfoTs of all WorldT-derived classes must register with this TypeInfoManT instance.
        cf::TypeSys::TypeInfoManT& GetWorldTIM();


        class EntityT;


        /// This class holds the hierarchy of game entities that populate a game world.
        /// The root of the hierarchy is the map entity, all other entities are direct or indirect children of it.
        /// The world also holds shared resources that all entities commonly use, such as the script state and the
        /// model manager.
        class WorldT : public RefCountedT
        {
            public:

            class InitErrorT;

            /// Flags for initializing a world from a map script.
            enum InitFlagsT
            {
                InitFlag_InlineCode  = 1,   ///< Normally, the `ScriptName` parameter to the WorldT ctor is a filename. If this is set, it is treated as inline script code.
                InitFlag_InMapEditor = 2,   ///< Whether the world is instantiated in the Map Editor. If set, only the static data will be loaded, initial behaviour is *not* run.
                InitFlag_AsPrefab    = 4    ///< This must be set if the map script is loaded for use as a prefab. Can only be used if `InitFlag_InMapEditor` is set as well.
            };

            /// Initializes the given script state for use with WorldT instances.
            static void InitScriptState(UniScriptStateT& ScriptState);

            /// Assigns the given world to the global "world" and loads the given script in order to initialize it.
            /// @param World        The world to init.
            /// @param ScriptName   The file name of the script to load.
            /// @param Flags        A combination of the flags in InitFlagsT.
            /// @throws Throws an InitErrorT object on problems loading the script.
            /// @returns The root entity as loaded from the given script.
            static IntrusivePtrT<EntityT> LoadScript(IntrusivePtrT<WorldT> World, const std::string& ScriptName, int Flags = 0);


            /// Constructor for creating an entity hierarchy (== "a world") from the given script file.
            /// @param ScriptState  The caller will use this world with this script state (binds the world to it).
            /// @param ModelMan     The manager for all models that are used in this world.
            /// @param GuiRes       The provider for resources (fonts and models) for all GUIs in this world.
            /// @param CollModelMan The manager for all collision models that are used in this world.
            /// @param ClipWorld    The clip world, where entities can register their collision models and run collision detection queries. Can be `NULL`, e.g. in CaWE or the map compile tools.
            /// @param PhysicsWorld The physics world, where entities can register their rigid bodies and run collision detection queries. Can be `NULL`, e.g. in CaWE or the map compile tools.
            WorldT(UniScriptStateT& ScriptState, ModelManagerT& ModelMan, cf::GuiSys::GuiResourcesT& GuiRes,
                   cf::ClipSys::CollModelManI& CollModelMan, cf::ClipSys::ClipWorldT* ClipWorld, PhysicsWorldT* PhysicsWorld);

            /// Returns the script state of this world.
            UniScriptStateT& GetScriptState() { return m_ScriptState; }

            /// Returns how many millimeters one world unit is large.
            /// Used whenever we have to deal with concrete units of measurement such as millimeters or meters
            /// (e.g. for physics computations, the acoustic Doppler effect, etc.).
            /// FIXME: The same constant is currently also defined (as `const double METERS_PER_WORLD_UNIT = 0.0254`)
            ///        in CollisionModelStaticT::BulletAdapterT, PhysicsWorldT::TraceBoundingBox(), and the Sound Systems.
            float GetMillimetersPerWorldUnit() const { return 25.4f; }

            /// Returns the root entity of this world.
            IntrusivePtrT<EntityT> GetRootEntity() const { return m_RootEntity; }

            /// Returns the ID that the next newly created entity should get.
            unsigned int GetNextEntityID(unsigned int ForcedID = UINT_MAX);

            /// Returns the manager for all models that are used in this world.
            ModelManagerT& GetModelMan() const { return m_ModelMan; }

            /// Returns the resource provider for commonly used GUI fonts and models.
            /// All GUIs that are created in this world share their font and model resources via the returned GuiResourcesT instance.
            cf::GuiSys::GuiResourcesT& GetGuiResources() const { return m_GuiResources; }

            /// Returns the manager for all collision models that are used in this world.
            cf::ClipSys::CollModelManI& GetCollModelMan() const { return m_CollModelMan; }

            /// The clip world, where entities can register their collision models and run collision detection queries.
            /// Can be `NULL`, e.g. in CaWE or the map compile tools.
            cf::ClipSys::ClipWorldT* GetClipWorld() const { return m_ClipWorld; }

            /// The physics world, where entities can register their rigid bodies and run collision detection queries.
            /// Can be `NULL`, e.g. in CaWE or the map compile tools.
            PhysicsWorldT* GetPhysicsWorld() const { return m_PhysicsWorld; }

            /// Renders this world.
            /// Note that this method does *not* setup any of the MatSys's model, view or projection matrices:
            /// it's up to the caller to do that.
            void Render() const;

            /// Processes a keyboard event by forwarding it to the entity that currently has the input focus.
            /// @param KE   The keyboard event to process.
            /// @returns `true` if the device has been successfully processed, `false` otherwise.
            bool ProcessDeviceEvent(const CaKeyboardEventT& KE);

            /// Processes a mouse event by forwarding it to the entity that currently has the input focus.
            /// @param ME   The mouse event to process.
            /// @returns `true` if the device has been successfully processed, `false` otherwise.
            bool ProcessDeviceEvent(const CaMouseEventT& ME);

            // /// Advances the world one frame (one "clock-tick") on the server.
            // /// It typically updates all game-relevant state that is sync'ed over the network to all
            // /// connected game clients.
            // /// EntityT::OnServerFrame() is called by this method for each entity in this world.
            // ///
            // /// @param t   The time in seconds since the last server frame.
            // void OnServerFrame(float t);

            /// Advances the entity one frame (one "clock-tick") on the client.
            /// It typically updates eye-candy that is *not* sync'ed over the network.
            /// EntityT::OnClientFrame() is called by this method for each entity in this world.
            ///
            /// @param t   The time in seconds since the last client frame.
            void OnClientFrame(float t);


            // The TypeSys related declarations for this class.
            /*virtual*/ const cf::TypeSys::TypeInfoT* GetType() const { return &TypeInfo; }
            static void* CreateInstance(const cf::TypeSys::CreateParamsT& Params);
            static const cf::TypeSys::TypeInfoT TypeInfo;


            private:

            WorldT(const WorldT&);              ///< Use of the Copy Constructor    is not allowed.
            void operator = (const WorldT&);    ///< Use of the Assignment Operator is not allowed.

            void Init();    ///< Calls the OnInit() script methods of all entities.


            UniScriptStateT&            m_ScriptState;  ///< The script state that this world is bound to.
            IntrusivePtrT<EntityT>      m_RootEntity;   ///< The root of the entity hierarchy that forms this world.
            unsigned int                m_NextEntID;    ///< The ID that the next newly created entity should get.
            ModelManagerT&              m_ModelMan;     ///< The manager for all models that are used in this world.
            cf::GuiSys::GuiResourcesT&  m_GuiResources; ///< The provider for resources (fonts and models) for all GUIs in this world.
            cf::ClipSys::CollModelManI& m_CollModelMan; ///< The manager for all collision models that are used in this world.
            cf::ClipSys::ClipWorldT*    m_ClipWorld;    ///< The clip world, where entities can register their collision models and run collision detection queries. Can be `NULL`, e.g. in CaWE or the map compile tools.
            PhysicsWorldT*              m_PhysicsWorld; ///< The physics world, where entities can register their rigid bodies and run collision detection queries. Can be `NULL`, e.g. in CaWE or the map compile tools.


            // Methods called from Lua scripts on cf::GameSys::WorldT instances.
            static int CreateNew(lua_State* LuaState);      ///< Creates and returns a new entity or component.
            static int GetRootEntity(lua_State* LuaState);  ///< Returns the root entity of this world.
            static int SetRootEntity(lua_State* LuaState);  ///< Sets the root entity for this world.
            static int TraceRay(lua_State* LuaState);       ///< Employs m_ClipWorld->TraceRay() to trace a ray through the (clip) world.
            static int Phys_TraceBB(lua_State* LuaState);   ///< Employs m_PhysicsWorld->TraceBoundingBox() to trace a bounding-box through the (physics) world.
            static int toString(lua_State* LuaState);       ///< Returns a short string description of this world.

            static const luaL_Reg               MethodsList[];  ///< List of methods registered with Lua.
            static const char*                  DocClass;
            static const cf::TypeSys::MethsDocT DocMethods[];
        };
    }
}


/// A class that is thrown on WorldT initialization errors.
class cf::GameSys::WorldT::InitErrorT : public std::runtime_error
{
    public:

    InitErrorT(const std::string& Message);
};

#endif
